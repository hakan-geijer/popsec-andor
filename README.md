# PopSec: Andor

A zine fusing popular culture and operational security by looking at the plot of the series Andor.

## (Anti-)Copyright

This work is in the public domain under a CC0 license.
