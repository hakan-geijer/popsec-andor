# 1:30

Luthen convinces Cassian to join him on heist revealing how much he knows about Cassian's past and also that whether Cassian can admit it to himself or not that he'll likely die fighting and stealing from the Empire.

# 6:32

ISB supervisor Dedra Meero arrives to work.
During a security briefing, supervisor Blevin discusses the botched arrest on Ferrix.
He mentions the stolen starpath unit recovered at the scene.

# 9:20

An Aldhani Cassian shaves.
Luten asks him for a fake name.
Cassian replies "Clem."

Cassian learns he's not going to be working for Luthen but rather someone else (Vel Sartha) who will "hate" the idea.

He offers Cassian a valuable gem as a down payment.

# 12:10

Luthen tries to sell Cassian's skills to Vel.
She replies that it's so soon with so few days until the mission that.
"It will tear the team apart."
The keep discussing, and Luthen says he's paying Cassian 200,000.
"We've been eating roots and sleeping on rocks for this rebellion, and now you've got a mercenary on board?"

# 14:20

Belvin release Hyne, Syril, and Linus from duty.
Morlana is now under Imperial control.

# 16:06

Cassian and Vel walk through the mountains.
Cassian asks "Who is [Luthen]?"
"You should have asked him that when you had the chance."
Vel clarifies that Luthen doesn't exist and that he's never to be mentioned.
They plan the lie they're going to tell the rest of the team.
Cassian learns they're not robbing payroll but a garrison.

# 17:43

In the ISB building, Heert briefs Dedra about the incident on Ferrix.
They track down the source of the stolen starpath unit and make plans how to take jurisdiction without sparking Blevin's interest.


# 18:43

Luthen arrives on Coruscant and puts himself into his formal wear including wig.
He practices his expressions and gestures of his public life.

# 19:40

Cassian and Vel take a break and discuss what the Empire has done to the local tribes.

# 20:40

Dedra confronts Blevin about obstructing her investigation.
He tells her to be careful about ladder climbing.

# 21:35

Cassian and Vel continue hiking.

At camp, Karis Nemik is sleeping on guard duty and awoken with a blaster by ally by Arvel Skeen.
He's criticized for sleeping on and how that could have killed everyone.
Arvel mentions that in other crews if he did that he'd be killed for a laugh.
"Don't tell Vel."
"I won't. You will."

They realize Vel is coming back with someone else in tow.
They alert the rest of the team.
The team is uneasy about a new face.

Vel introduces Cassian and explains the lie.
Arvel says "It's a bit late for surprises."
Vel ignores it and introduces the team.
Taramyn Barcona is particularly not pleased and wants to talk to Vel.
Nemik, the youngest, is more friendly.

# 24:42

Syril arrives home at Coruscant and gets berated by his mom after moving back in with her.

# 26:44

Cinta cleans Cassian's wounds.
He hides Luthen's gem.

In another hut.
Arvel   : So you don't know him?
Vel     : I know that we need him. That's all I'll say. Anything else is a violation of security.
Taramyn : No, he's got brass. You... You can feel it. And we can use a hand. But this late in the game?
Vel     : As opposed to when?
Arvel   : You trust him with our lives?
Vel     : That's my call to make.
Nemik   : He's committed. I'm feeling that. (pause) I want to.
Arvel   : Feel what?
Nemik   : His belief in the cause. When it comes down to it, that's all I need to know.
Taramyn : Vel. Vel!
Vel     : I trust him. Okay? Alright?

# 27:52

On Coruscant, Mon Mothma arrives at Luthen's antiquities shop.
His assistant Kleya alerts him and says "New driver."
"Anyone we know?"
"Never seen him."
"Chandrilan?" (Mon's homeworld)
"One would think"

They put on a show for the driver.
They start an innocuous conversation.
Kleya distracts the driver with conversation.

He inquires whether Mon can deliver money.
She says it's dangerous to move money.
"They're watching me now.
"Oh, they're watching everyone."
"This is different. They're everywhere. There's a new spy every day at the senate. I visit the bank, they're all new faces."
"You got a new driver."
"I feel under siege."

"I found someone I think can help me."
"Someone who? To bring into the circle? No."
"I know what I'm asking."
"No. We're vulnerable enough. We need funding, not more people to worry about."
"Don't lecture me on vulnerability. No one's more at risk than I am. You think I haven't thought this through? I'd be the first one to fall."

# 32:07

An Imperial soldier, Lieutenant Gorn,  arrives at the camp.
Cinta says "He's with us."
He's upset about the addition.
Too little time, Cassian is wounded.
He's a stranger who could endanger the mission and Gorn's life.
Gorn still wants to know who Cassian is.

# 34:07

In Mon's house, she deals with family issues with her husband Perrin.
He wants to have dinner with people she finds distasteful because they work against her humanitarian issues.

# 36:12

Vel explains the plan and shows a diorama.
Cassian starts to see how slim the chances of things working out are.
They explain the plan will have cover as it passes through a comet trail (?) that will let them escape.
"Are you in all the way."
"Let's get to it."

# 39:37

Dedra and Blevin argue in front of Partagaz about who has jurisdiction.
She says she sees signs of coordinated activity that could be the seeds of a rebellion.
Partagaz tells her to keep to her assigned work.

# 42:03

Cassian eats dinner with the team.
Gets given assignments.
The other watch how he reacts to his work.
