# 8:22

<!-- TODO hidden until the last possible second -->
Wearing imperial gear under their Dhani clothing, the four lads trek down to the valley.

# 40:32

After the raid, Arvel and Cassian talk while Nemik gets surgery and Vel stays by his side.
Arvel suggests Cassian and he take the cash and run, leave everyone behind.
He admits his story about his brother was a lie.
He's only out for himself.
Cassian shoots him.

He tells Vel he's leaving with his earnings.
She doesn't believe Arvel would betray them.

# 44:42

At the ISB HQ, Dedra and others meet with others for retaliation plans.

# 45:06

Mon presents her proposal for limiting the Emperor's powers.
News of the attack ripples through the senate, and she's cut short.


# 45:49

Kleya helps a customer.
Another asks if they have anything from Aldhani.
Luthen is caught off guard, but after hearing it was in the news goes to the back and laughs.
