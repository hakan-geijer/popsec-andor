# 4:19

ISB officer gives a speech about how harshly the Empire will respond to the theft.

# 6:30

Dedra tells Heert that we're playing straight into their hands.
Dedra: "We're treating what happened at Aldhani as a robbery."
Heert: "What is it?"
Dedra: "An announcement"

# 6:47

Luthen listens to a pirate radio announcement about the heist.
Mon arrives (without warning).
She is worried that Luthen was behind the attack.
He explains that they needed money not a new face.
She has no appetite for the violence that comes, she only thought they were building a moment.
"We need the fear, we need them to overreact."
...
"We can't hide forever."
<!-- TODO actions that are always hidden won't change anything -->

# 11:07

Syril interviews at the bureau of standards.

# 12:46

Kleya walks around Couruscant  following some chalk markings.

# 13:43

Dedra requests data under discretely from an attendant.

# 14:42

Cinta uncovers a speeder on Aldhani and before mounting sees a star destroyer flying over head.

# 15:05

Kleya meets with Vel at an overlook.
Kleya: "I shouldn't be here."
Vel: "Neither of us should."
They cleaned up the evidence.
They discuss the loss of the comrades

<!-- TODO your loose lips directly implicate others -->
Kleya: Receiving messages is just as dangerous as sending them

Kleya says Cassian is a loose end, Vel is hesitant to kill him.

# 17:12

Cassian returns to Ferrix.
Maarva says he can't stay and that it's not safe.
Explains the occupation.

Maarva says that Timm turned him in and that Timm is dead.
Maarva repeatedly tells him he has to leave for his safety.
Cassian says they can both clear out forever.

# 20:14

Mon hosts a party in her flat.
She meets her "contact," an old friend from her home world.
She inquires if she can confide in him in regards to a great secret
Tay: Would you want to do that?
He says that her closeness to the empire means she shouldn't confide because of how "far afield" his politics have gone from allegiance to the Empire.
She admits her whole life and image is a lie.
She asks for help getting access to her money.
He asks why is she raising funds.
He says why?
"You're better off not knowing"
Says Perrin isn't to be trusted.

# 26:26

Cassian knocks at Bix's work.
She tells him that it's not safe for him to be back.
She says that people blame Cassian for the occupation because of his actions.
That people would turn him in.
They argue a bit.
He says he's leaving for good and pays off his debts.

# 30:33

He hears stormtroopers talking and gets spooked.
Has a flashback to when the Imperials arrived.

# 31:33

Cassian arrives at Maarva's hovel.
She says she's no leaving.
She says she's a rebel and she's talking on the Empire.
The attack at Aldhani inspired her but she doesn't know he did it.
She thinks he can't be revolutionary but wishes him well in finding his own peace.

# 36:13

<!-- TODO know how enemy organized, blind spots because of bureaucracy -->
Back in ISB HQ, Blevin accuses Dedra of overreach.
In the discussion, she points out that rebels don't care about their jurisdictions.
"lines on a map."
After the exchange, Partagasz give Ferrix to Dedra.

# 40:49

Cassian hangs on a beach at Niamos.

Some criminals run and are chased by troopers.
Cassian gets spooked and takes off.
This causes troopers to treat him suspiciously and stop him.
He acts too suspiciously.
Gets questioned and arrested.

# 44:41

In a courtroom he is sentenced to 6 years, not just 6 months for even the fabricated crimes.
<!-- TODO even lying low, oppression gets us all -->
