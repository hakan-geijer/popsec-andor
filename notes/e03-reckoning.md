# 2:30

A young Cassian enters the freighter and sees more corpses and is started by the automated doors.
He sees his reflection and after studying it, attacks it.

# 4:28

Cassian watches workers including Brasso dismantle starships.
He talks to Brasso about money coming in and having to leave.
Brasso asks "Where are you going?" and Cassian replies "It's better you don't know.

# 5:48

Luthen arrives in Ferrix City.

# 6:29

Bix watches for Luthen's arrival and catches him as he enters down.
She starts with "There's been a complication. I missed the window to call back."
He replies "Calmly."
She explains the bulletin, and he says "I've seen it. The bulletin doesn't mention him by name. It says they're looking for someone from Kenari. His Imperial record says he's from Fest."
She says "Yeah, that's always been his story."
"Has he been identified?"
"I don't think many people know him."
"He killed these men on Morlana?"
She nods.
"Then we'd better be quick about it."
He asks about Cassian's location and says "Do you trust him?"
"He'll be there."


# 7:30

Pre-Mor transport ship arrives and dropships launch.

# 8:22

Maarva and Clem arrive in the freighter with a functioning B2EMO.
They hear the noises of Cassian.
Clem is hesitant about continuing in a dangerous situation with more unknowns.

# 9:42

Dropships are making their final approach to Ferrix City.
Syril and Mock's ship lands on the outskirts.
They get long looks from workers who aren't happy to see them.
Brasso's work team notes that guards don't come around often and that they're hunting for someone.

# 11:49

Cops arrive at Maarva's house with a warrant.
They begin searching.

# 13:24

Cassian radios B2EMO and Syril overhears it.
Mosk traces the signal.
A crowd grows outside the hours.
Mosk suggest leaving two, not one guard.
The rest leave.

# 14:10

Luthen arrives at the location and inquires about his idenity.
They establish that they're both alone.

# 14:30

The guards leave Maarva's hovel to an angry crowd.
They have a 10 minute walk to Cassian's location.

# 14:49

Luthen and Cassian begin negotiating price.
They don't trust each other, questioning if each brought money/goods, if it's a scam, it it works.
Cassian says "Has Bix ever burned you before?"

# 15:44

Salman who saw the raid on Maarva's hovel tells Bix about it at work.
She realizes someone ratted him out, and she makes to leave to warn him.
Timm stops her.
She realizes Timm is the rat.

# 16:50

Cassian shows Luthen the starpath unit.
"Where'd you get it?"
"What difference does that make?"
"I see three choices. Either you're an Imperial spy, you're fronting for the person I really want to speak to, or you're the thing itself."
He mentions he knows the game he and Bix play to get ships' quartermasters to leave valuables they can take.
"No, I went in and got this myself."
He pays Cassian to tell him how he got it out of a sealed naval base.

"You just walk in like you belong."
"Takes more than that, doesn't it?"
"What? To steal from the Empire? What do you need? A uniform, some dirty hands, and an Imperial tool kit. They're so proud of themselves they don't even care. They're so fat and satisfied, they can't imagine it."
"Can't imagine what?"
"That someone like me would ever get inside their house, walk their floors, spit in their food, take their gear."
"The arrogance is remarkable, isn't it? They don't even think about us."

Luthen reveals he knows of Cassian.
He gives some details, and Cassian panics and pulls a gun.

# 20:11

Guards are walking through the city.
People start banging pipes and scrap as a warning to other and to rally people to fight back.
Syril asks "What is all this?"
Most replies "Intimidation, sir. Bluff and bluster."
People start closing up shops.

# 21:07

Cassian asks how he got here and suggests Luthen is the Imperial spy.
A device in his pocket beeps.
Luthen asks if Cassian is carrying a commlink.
Cassian hands it over, and Luthen smashes it.
<!-- TODO why would he hand it over? foolish or prudent? -->
"Rule number one. Never carry anything you don't control."

When the realize they're surrounded, Luthen prepares to blow the doors.
"Rule number two. Build your exit on your way in."

# 25:21

Mosk radios to the team who snatched Bix and asks for their location.
"There's not signs. I don't know what street."
<!-- TODO dangers of bad planning -->

# 26:47

One soldier reports to Mosk that they have explosives, and Mosk is shocked to hear "they."

# 30:42

Brasso had attached a cable to a dropship when it was left unattended.
When it takes off, it crashes.
Most and the other guards see this.
"What that him?"
"But that would put them behind us?"

Cassian escapes from the wreckage.
Maarva cries.
Bix is freed.
Brasso looks sad.

In parallel as Luthen rescues Cassian, we flash back to Maarva and Clem rescuing a young Cassian.
