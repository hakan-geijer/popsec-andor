# 02:36

Syril wakes up at home with his mom.
She criticizes him for having no prospects.
She makes plans to call in a favor on his behalf.

# 04:54

Cassian wakes up and sees his possessions are missing.
He sees Arvel Skeen standing in front of his possessions.

Arvel: It's all there. Vel asked me have a look.
*pause*
Arvel: I think she's having second thoughts. Go talk to her if you want. She'll be up soon.
*pause*
Arvel: You didn't come with much. That plus the bad arm. It's pretty clear you left wherever you were in a hurry.
*picks up blaster*
Arvel: Corporate issue. Interesting.
Arvel: There's no sense bein' upset. You're luck to be alive right now. We've been down here for months, and the stakes are high.
...
*pointing at blaster*
Arvel: And whose is this?
Cassian: Didn't get a name.

Cassian sees Arvel's prison tattoo, and Arvel sees him seeing it.
They talk about prison.

Arvel: The axe forgets, but the tree remembers. Now it's our turn to do the chopping.
Cassian: So that's it. That's why you're here? Revenge.
Arvel: Yeah, that's good enough for now. And you.
Cassian: I was told I could help.
Arvel: Yeah, but you won't say by who.
Cassian: Working with other people is never easy.
Arvel: Yeah. I didn't mark you for a team player.
...
Cassian: And the lieutenant?
Arvel: Without him, there's no plan.
Cassian: He could be walking us right into a trap.
Arvel: They would have taken us down by now. Or maybe that's what you're here for.

# 08:41

Mon and Perrin have breakfast.
Mon and Freida (daughter) have a tiff about some plan with their personal lives.

# 10:36

Nemik gives Cassian some breakfast.

Nemik pulls out an old navigational tools.
Old and study. Can't be tracked, and can be repaired.
"Once you've mastered it, you're free. We've grown reliant on Imperial tech and we've made ourselves vulnerable. There's a growing list of things we've known and forgotten, things they've pushed us to forget."

Arvel mocks Nemik a bit for his true belief and his love for ideas.
Nemik explains himself and the importance of revolutionary knowledge.

Arvel: I'd like to head what Clem believes.
Cassian: I know what I'm against. Everything else will have to wait.

Cassian gets called off while Nemik continues to proselytize.

Nemik: You don't trust him.
Arvel: I barely trust you.

# 13:02

Vel and Tamaryn show a model of the facility and discuss the escape.
Cassian realizes the don't know how to fly it, and they realize he knows his shit.

# 14:46

Tamaryn shows the layout of the camp as matching the layout of the actual facility.

# 15:34

Tamaryn back on base chews out two soldiers for not cleaning

# 16:23

Tamaryn briefs Cassian on his identity.

# 17:01

On Ferrix, Blevin surveys the hotel they're converting to their HQ.

# 17:54

Tamaryn drills them on marching.
There's a slight beef between Cassian and Tamaryn about marching.
A TIE fighter buzzes them and they hide the guns.

# 20:17

Tamaryn patrols the base and talks to other soldiers.
The soldier is casually racist against the locals.
Tamaryn is not amused.

# 21:34

Back at the camp, they prepare.
Tension as Arvel touches Cassian's stuff.

# 22:55

Dedra investigates things with her partner.

Dedra: It's too spread out to be organized
Heert: But you dn't believe that
Dedra: I know this. If i was them, this is how I'd do it. I'd spread it out. Never climb the same fence twice.
Heert: It's too random to be random.

# 24:32

At camp, the rebels burn traces of their operation and prepare for bed.
In the morning they move out.

# 25:51

Syril and his mom argue about the favor being called in and Syril's resentment about not having agency.

# 26:52

Vel grills Cassian on ops details.
Cassian asks why Tamaryn is in on the rebellion.
He loved a local, lost his promotion and the woman, then turned.
"Everyone has their own rebellion."

# 27:42

Tamaryn discusses ops with the soldiers in the garrison.

# 29:02

Arvel holds a knife to Cassian's neck.
He snatches the gem from his neck and announces that Cassian has been hiding something.

Arvel: I've topped out on questions, Vel. I have reached my limit.
Arvel: He won't say why he's here, where he's from. He won't way what he believes in, and now this? Who brings a treasure to a robbery?

They start to move out.

Cassian says he's a mercenary.
Says they're flipping because they're losing their never.
"I don't want to be looking over my shoulder."

# 32:38

Mon and Perrin ride in their car.
Mon hides parts of her life from Perrin.

# 33:39

The rebels arrive at their positions around the garrison.

On Coruscant, Syril looks at a hologram of Cassian.

# 35:06

Arvel talks to Cassian and explains his hated of the Empire.
Brother, farmer, was dispossessed by Empire, killed himself.
This is his apology to Cassian.

They split up for the night.

# 37:28

Luthen listens in on his pirate radio.
Kleya points out that he hasn't turned it off.

"Have you checked your walk-away pack."

Kleya: Vel's the only one who traces back.
Luthen: No. The thief. Andor. I wasn't careful.
Kleya: You wanted this to happen. This is what it took. It was never going to be perfect.
Luthen: I wanted it too much.
