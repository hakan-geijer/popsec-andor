# 2:46

Cassian and Melshi climb a cliff avoiding Imperial patrols.

# 3:31

Brasso, B2EMO, and friends clean out Maarva's hovel after her death.
B2EMO is sad :((

The ISB agent watches.
So doe Cinta.
He's in her cafe but they don't recognize each other.
They exchange a few words.

# 6:17

Cassian and Melshi watch two local fishers and plan to steal their ship.
They get caught in a net.

# 7:35

Dedra talks with the local ISB agents about Maarva's death.
Dedra tells them to let the locals have a funeral for Maarva (small, short) to kettle them and also bait Cassian into showing up.

# 8:34

The fishers complain about the Empire, then help them escape.

# 10:31

Kleya cleans coins in the shop
Vel visits.
Kleya is upset she's there.
"We have rules for a reason. You don't seem to understand that."

Vel asks what Kleya has done lately.
Says keeping many plates spinning.

Vel says she's on the way Ferrix for Cassian's mother's funeral.

# 13:44

ISB agent watches Brasso arrive at Maarva's.
Cinta too.
Brasso spends the night.

# 15:34

Bix is hallucinating after torture.
Her captors ask if Anto Kreegyr is the man she introduced to Cassian.
She sobs.

# 17:39

Vel and Mon watch Leida practice courtship rituals with friends.
Leida has sought out an elder to teach her the Chandrilan ways.
Mon says "I'm in so much trouble, Vel."

She explains her financial issues.
And how she went to Luthen then got Tay involved.
How much is missing, how bad Tay say it was.
The timeline.
Luthen doesn't really know.
She says she's found a solution.

# 21:53

Eeda wakes Syril for a call.
Mosk says that he got a tip off about the funeral.
Eeda mocks him after the call ends.

# 24:00

gack on Niamos, Cassian picks up his stash of guns, cash, and Nemik's manifesto.

# 24:49

Luthen visits Saw.
Saw says he's in for Spellhaus, lists his conditions, asks Luthen to talk to Kreegyr.
"I think not."
"Why? You were so eager for it."
"ISB knows Kreegyr's plan. They'll be waiting for him."
"And how do we know know this?"
"I won't tell you that."
"And Kreegyr, he doesn't know... How do you know I won't tell him?"
"I don't. I don't know what you'll do... It's far from ideal for either of us."
"You're willing to burn him."
"You're the random factor."
"It's 30 men."
"Plus Kreegyr."
"So you know he's doomed... Which means either you're ISB or you have someone inside that you're protecting."
"Or I'm just a very good listener."
"You think it's worth losing Kreegyr?"
"I did. I'm not sure right now."
"What if it was me instead of Kreegyr? What would you do?"
"Kreegyr doesn't know me. I'm not vulnerable if he's captured."
"Surely you've met him."
"I've me him. I've been in a room with him, but he doesn't know that. We send people. We drop supplies. We have special radios. He cant hurt me."
"Like I can."
"It's your decision, Saw... It's your decision. But know the choice. Do we let Kreegyr go down and play the long game, or do we warn him and throw away a source that's taken years to cultivate?"
"You have people everywhere, don't you?"
"You're avoiding the question."
"All of your sources..."
"Try to concentrate."
"There's someone with Kreegyr? Someone at ISB? Maybe there's someone here with me."
"You're wasting time."
"Why wouldn't you?"
"You're avoiding the choice."
"Who is it? Who is it?"
"It's Tubes."
T: "What is this? Nonsense!"
"He's my man. He tells me everything. Tell him. He deserves to know."
"What is this?"
T: "He's lying. I have only allegiance to you!"
"What kind of game is this? ... There's no way out alive. Of that you must be sure."
"I'm doing this so you'll listen. So, listen... Kreegyr goes down, the ISB will feel invincible. They'll feel untouchable. We'll have a clear field to play. The alternative, Kreegyr pulls out, we wave him off. They'll know, they'll have to wonder. They'll trust nothing. Just like you're doing now... If I were ISB, Saw, why wouldn't I just send you out there with him? ... I didn't want you to have to make this choice."
"Thirty men?"
"Plus Kreegyr."
"For the greater good."
"Call it what you will."
"Let's call it war."

# 29:52

Syril steals some money from his mother.

# 30:29

Kleya radios to Luthen with an update about Saw.
They speak in code.
She warns him of going after Andor.
He insists.

He's caught in the snare of a random patrol.
Kills the fighters and escapes.

# 34:59

Cassian radios Xanwan, tells him to give Maarva his kind words.
He tells Cassian that she died.

Cassian and Melshi talk.
Split to double chance to ensure someone makes it to get the word out about the prisons.
Cassian gives him a gun.
They part ways.
