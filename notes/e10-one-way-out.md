# 2:43

Ulaf's corpse is transported out through the factory floor.

# 3:07

Cassian tells Kino they should "go" tomorrow.
He says they will never have a better time because the guards are outnumbered and afraid.
He knows whatever is being produced is highly needed, so to avoid being surprised, they're likely going to get more guards.
They need to act before that.
"Every day we wait they grow stronger."

In the cell block, the others ask
Kino is hesitant, but Cassian explains.
They don't believe him until Kino snaps and confirms too.

# 6:39

In the ISB HQ, Dedra and others confirm that Kreegyr's men are aware of their plan.
Supervisor Lonni Jung says that the most normal thing the Imperials could do would be to lightly investigate what happened.

# 7:31

In prison, Kino says there's only one way out.
"I'm gonna assume I'm already dead."
The shift begins after a strict hot bunk transfer between the day and night shift.

# 9:01

Jezzi tells a doctor that Maarva isn't taking her meds.
Cinta overhears.
An ISB agent does too.

# 9:43

Davo visits Mon and Tay.
She asks about the fee.
He wants none, but she insists.
He says she's not having a fee, and his "price" is to set up his son with her daughter.
Not betrothal, only an introduction.
She kicks him out.

# 14:16

Kleya says there's a mark on the fountain, a rail is missing.
Some secret agent needs to meet.
Kleya suggests Luthen fucks off and get safe because of the suspicious timing.
He insists on going.
"If it's a trap, we've already lost."

# 15:03

In prison, they're scheming and sabotaging.
During the prisoner transfer they revolt.

# 26:05

Cassian gives Kino access to the main PA in hopes of rallying everyone to revolt.

# 31:02

On Coruscant, Lonni goes to meet Luthen who he hardly knows.
Luthen mentions he knows of Lonni's daughter.
Lonni says it's unfair, how it feels to be watched by him.
He warns Luthen of Dedra rising the ranks and is looking for Axis.
"Well, this is good."
"Why is this good?"
"Because she's wasting time."
Luthen denies involvement with Aldhani.
Main reason he is there is to mention the captured pilot and the ISB knows about Kreegyr and Spellhaus.
Lonni wants Luthen to warn Kreegyr to save the rebels, but Luthen wants to sacrifice them.

Luthen asks why they're really there.
Lonni wants out.
Says he's been alone (lonely).

Luthen says they've been feeding him info to make him rise the ranks.
Tells Luthen he's trapped because they (rebels) need him so much.
Lonni asks what Luthen sacrifices.
"Everything!"
Calls Lonni a hero, asks him to stay.

# 38:33

Cassian and Melshi run off into the night.
