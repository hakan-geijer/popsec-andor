# 2:04

Cassian's tribe is on their way to the crashed freighter.

# 5:32

In the office, Bix works on a terminal.
When Timm comes in and ask what she's doing, she quickly powers off the terminal's monitor.
When she leaves, Timm powers the monitor back on to see what she was looking at.
It's the security bulletin from Pre-Mor inquiring about someone matching Cassian's description.
He gets a look in his eye.
<!-- Note: others seem to disagree that she saw this alert but why else would she rush out to talk to him -->

# 6:32

Cassian arrives at Maarva's flat where, before he walks in, Maarva shushes B2EMO and tell it not to say a word.
After telling a lie about where he was and why he's injured, Maarva tells B2EMO to replay the Pre-Mor bulletin.
Maarva asks "Who else knows? ... That you were born on Kenari? ... Who else have we told"
Cassian says "I don't know."
She says that they've been careful to lie about his origin saying he was born on Fest.
When questioned about whether he's said anything other than Fest, he says "Officially, no. I don't think so. But people, yes."
She asks "How many?" as he points out that she's done the same.
He says "I don't know, it's not something I've been keeping track of."
She says "Everyone I've told is dead."
He says "What about Jezzi? Sammo?"
She says "That's family."
He says "If we're making a list, we're making a list."
She asks about all his romances and eventually says "But then who told these Pre-Mor bastards about Kenari?"
He says "That would be me" and after arguing with B2EMO says "I messed up."

<!-- TODO: asset management, knowing one's risk profile by knowing who knows what -->

# 9:28

Cassian joins Bix at a cantina.
She invited him there because she "wasn't sure what [was] safe."
He explains why he's being pursued and learns that the fence is coming the next day.
While leaning in close to thank her for her help, the camera shows that Timm is watching them from across the bar having likely followed Bix because he was suspicious about her relationship to Bix.
Timm leaves and places a phone call to Pre-Mor to snitch on Cassian.

# 11:19

The call reaches the security office in Morlana One.
They get his name, face, and wrap sheet, but it says he's from Fest but not Kenari.
The hostess from the Brothel comes in as a witness wit the implication that she would ID Cassian.

# 12:20

On Ferrix, Bix goes to Timm's place and spends the night.


# 13:52

In the security office of Morlana One, Sergeant Linux Mosk introduces himself to Syril.
He has the same dedication and enthusiasm for law and order, for the job.
The discuss the need to quickly chase down Cassian.

# 15:58

Cassian preps his stolen item and gun for the next morning.

# 17:04

On Kenari, the tribe approaches the wreck and one takes the lead to investigate.

# 18:41

The fence Luthen Rael starts the landing sequence for Ferrix and lands a ways away from the shuttle pickup due to the safety of being so far out.

# 20:05

Bix wakes in Timm's bed.
Timm is awake already watching her because he couldn't sleep.
After asking him to open the yard on his own, she dresses and joins him in the kitchen asking if he's okay.
She senses something is off and he deflects saying he's just tired.
She says she's got errands to run when asked why she won't be able to help.
Timm is suspicious.

# 21:10

Cassian prepares comms with B2EMO as part of the plan to sell the item to the fence and stash some credits for Maarva.

# 23:06

The Kenari tribe hides in the trees while one approaches the wreck.
She pokes one body, and it doesn't move.
She pokes one handing out of the ship, and it falls out dead.
Behind her, one gets up and fires killing her.
From the trees, they use blowdarts to kill the assailant before grabbing her body and running off.
Cassian looks at the ship and stays beind with the intention to go in.

# 23:48

Cassian visits Xanwan who says he's looking up Kenari, presumably after seeing the bulletin and not knowing where it was.
Xanwan asks Cassian if he knows anyone from Kenari.
He then asks about an immediate departure and notes that the price needs to reflect Xanwan's discretion.

# 27:19

Mosk briefs the other guards on how to capture Cassian.
He notes that Cassian is armed but that they have the element of surprise.
He also says that "there may be some local residents who are less than enthused with [their] presence."
The guards laugh about this.

# 29:57

While taking the shuttle to town, a chatty elderly gentleman asks what Luthen's line of work is.
Luthen's scowl and silence are clear, and the man jokes "who knows who you're talking to these days."

Down below, Cassian hurries through a scrap yard.
