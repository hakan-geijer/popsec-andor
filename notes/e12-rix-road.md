# 2:16

Wilmon Paak makes a pipe bomb.
Dedra arrives on Ferrix.
Bix fidgets in her cell.
Dedra wants to take an inconspicuous walk around town.

# 4:10

Xanwan meets Brasso to say he spoke with Cassian.
Nurchi sees this conversation.
Xanwan walks off.

# 5:11

Corv (the ISB agent) walks around with Dedra.
He's tailed by Cinta.

# 5:45

Xanwan has a casual been with Nurchi.
Nurchi tries to pull info from him after liquoring him up.
"You know something I don't, do me a favor. Keep it that way."_

# 7:36

Mon waits for Perrin in the airspeeder after some social event.
She asks Kloris (driver) for privacy.
She gets mad at Perrin for illegally gambling.
"This is wrong, Mon. This is people trying to take you down by coming after me."
...
"Where would I get the money."
"That's the question that scares me the most."
"Someone's lying to you."
"On that, we can agree."

# 9:40

Cinta arrives home to Vel and starts explaining why she didn't pick her up from the starport, following some ISB agent.
Vel is upset Cinta is so driven.
Cinta explains the importance of her absence.
Vel is bothered even if she knows Cinta is doing the right thing.

# 11:08

Cassian suspiciously (you idiot) runs through the streets.
He visits his father's grave.

# 12:33

Hops the fence into Bix's workshop.
Pegla is there.
He asks why, then about Bix.

# 13:39

NEMIK'S MANIFESTO AHHHHH

We see Bix in her cell.
Luthen watching Ferrix city.
Cassian listens to it in his old hideout.

# 15:00

ISB briefs Dedra.
They expect 40 people who are "slow" after lunch.

# 16:16

At ISB HQ, Kloris briefs Blevin on Mon and Perrin's fight.
Lagret tells them to come watch the battle against Kreegyr. 

Kreegyr and everyone is dead.
Dedra is upset that there were no prisoners, but Partagaz justifies it as revenge.

# 18:04

Luthen rides in to town.

# 18:30

Corv keeps watch on Maarva's hovel.
He realizes that the person who left her hovel wasn't Brasso but a body double.

Cassian is in a tunnel and greets him.
Brasso says he shouldn't be there.
Cassian feels bad for himself for not being there, then relays a message of dis-guilting him.
And also a message of encouragement.

Pegla signals for him to go up, and Cassian says he's going to rescue Bix.

# 21:18

Linus and Syril fly in together.

# 22:00

Nurchi watches Pegla.

Luthen meets Vel.
Talks about killing Cassian.
Thinks they have a few hours, but the signal bell starts right after.
The Imperials are confused too.
Cassian watches the hotel.

# 25:10

People start playing music and marching toward the hotel.
It is far more than 40.
Wilmon joins.
So does Corv with Cinta tailing him.

Imperials panic.
Cassian watches.
Bix listens.
Cassian sees Luthen and realizes what it means.

# 27:41

Nurchi rats to Corv and asks to be "arrested" as cover.
Cinta sees this.

Syril and Linus can't get through the police line because they're not in uniform.

Dedra makes plans to snatch Cassian using Nurchi's tip.

Syril sees Dedra and gets excited(?).

Cassian escapes, then makes it in to the hotel.

# 32:30

Maarva's FTP speech.

# 37:29

Bix is not lucid when Cassian reaches her.
Wilmon yeets a bomb.
Riot goes nuts.

Corv notices Cinta is following him.
Confronts her.
Gets fuckin' shanked.

# 42:48

Syril rescues Dedra after she's almost killed by the crowd.

Luthen watches the riot from a distance then leaves.

Vel and Cinta clean out their house.

Cassian sends his friend off in a ship but doesn't stay with them.

# 46:55

Mon, Perrin, and Frieda welcome Davo, his wife, and son.

# 47:30

Linux drinks in a corner.

Luthen rides back to his ship.

# 47:49

Luthen realizes something is off in his ship.
Turns around to see Cassian.
Cassian confronts him about wanting to kill him.
He joins the rebellion.
