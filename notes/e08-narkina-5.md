# 2:32

Transport from Niamos to prison.

# 3:35

Syril is picked up from work for interview by the ISB.

# 7:31

Dedra presents her analysis to the ISB board.
They plan to surveil Ferrix to snare Cassian to find Luthen (who they know as Axis).

# 9:25

The prisoners arrive to the penal colony.
They are shown the electric floors meant to control them.

# 11:47

Dedra tries to get more info from Syril.
He's useless, but eager to help.
She spurns his enthusiasm.

# 14:04

While being admitted to his work group, Cassian takes note of the facility and the guards.
He realizes they're understaffed and sloppy.
Kino Loy, the kapo, introduces himself and give Cassian a rundown of the way the factory works.
At his table he is introduced to the people he'll work with.

# 20:43

Mon and Perrin are hosting a party, Tay arrives to talk to Mon.
The three of them exchange pleasantries.

# 23:01

Cassian finishes his first shift with a look of shock on his face.
He sees one of the men from his shift using sign language to communicate with another man on another floor and realizes there's an internal comms network for the prisoners.

# 24:22

The guests at Mon's party talk about supporting the Empire and order.

# 25:12

The crew gets back to their cell block.
Jemboc asks Cassian, going by Keef, what he did to get such a large sentence.
He is evasive.
Everyone else listens in impatiently.
They want to know why everyone's sentences doubled a month ago.
They now there's a new law (PORD) and want to now what's going on outside.
They now something happened at a garrison and feel they're being punished for what someone else did.
Someone says in desperation that they're never getting out.
Kino intervenes, presumably to salvage morale to keep the number up.

# 28:37

At Mon's party, she asks Perrin where Tay is.
He seems suspicious or her interest in Tay.

# 29:59

Cassian thinks a bit in his cell then goes to sleep.
Wakes. Eats. Is cleaned in a group shower.
The eldest of their crew shows signs of struggling.

# 32:52

Brasso walks to Maarva's where Bix is caring for her.
Maarva is sick.
She also wants to sneak rebels into the hotel with the Imperials.
Bix asks if Brasso knows where Cassian is. "No."

Vel and Cinta watch the two of them talk.
They talk about watching and how "the struggle" always comes first.

# 36:38

She goes to Salman Paak to use his radio to reach out to Luthen again to try to find Cassian.
"Not sure that's a good idea."
He agrees to break comms silence.
She makes a call.

# 37:29

Kleya hears the call.

K: It's the shop owner on Ferrix. She's trying to find Cassian Andor. His mother is ill.
*pause*
K: We're not answering. We can't.
L: She could point us in the right direction.
K: She's asking us.
L: She knows more than we do. So much more. She might have a lead.
K: More likely it's the ISB working her radio.
L: You're guessing!
K: And you're slipping!
*pause*
K: We're shutting down Ferrix. The code, the frequency, all of it.
   I'm thinking clearly, and you're not.
*holds mic*
K: Tell me to shut it down.
L: It's Andor. Knowing he's out there, knowing me, not knowing what he knows.
   I took him on the Fondor. Was I insane?
K: You were desperate for Aldhani to work, and it did.
   And we'll find him, just not like this.
L: Vel was out hunting. She and Cinta. Are they in Ferrix yet?
K: I'll have a listen. We're being extremely careful with it.
L: The woman's name is Bix Caleen. Vel could have a look if it's safe. The know what they're doing.
*pause*
L: I'm not slipping, Kleya. I've just been hiding for too long.
K: It's all different now. We're going loud. Vulnerability is inevitable.
L: I'm not slipping.
K: I know. I just need you to wake up. There's a lot to do.
L: Shut it down.

# 29:23

The sun is rising and Bix keeps calling.

# 41:40

Bix wakes up and sees people gathering.
She hears that Imperials are raiding Paak's yard.
She goes to investigate.

An officer recognizers her and has her arrested.
Brasso stands in the way to block the cops.

# 43:41

<!-- TODO no reason for fame, no reason people should know what you did -->
Luthen visits Saw Guerra.
He tricks Saw into believing that he believes that Saw pulled off the Aldhani raid.

He's there to sell him arms.
They argue because Saw doesn't want to work with Anto Kreegyr, put his people at risk for someone else.
Saw explains that his goals aren't anyone else's goals.
Names many other beliefs/orgs/groups. "They're all lost."
Then says "What are you Luthen, I've never really known."
Calls the differences Saw sees "petty."
Declines the offer for parts.

# 47:18

Bix is led to the Imperial hotel.
Dedra lets her see the tortured Salman.
Then starts interrogation.
