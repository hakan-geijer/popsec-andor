# 2:32

Dedra interrogates Bix.
She lists of what she knows about Salman and "the buyer."

# 8:06

Cassian works with his crew.
Ulaf (old guy) has 41 days left.
New people are arriving.

# 9:25

Bix' torture begins.

# 12:08

Cassian, seemingly with the others, is engaged in sabotage of the facility.
With another they observe the delivery of another inmate.

# 14:16

Dedra continues to interview/torture Bix.

# 14:46

Cassian keeps working.

# 14:28

We learn Salman will be executed.

# 16:00

Mon Mothma addresses the Senate to keep a small amount of checks against the Emperor.
She gets booed.

# 17:20

Something is wrong in the prison.
"Something's really wrong on level 2."
Poor information transmission between levels.

# 19:20

We learn Vel knows Mon and is her cousin.
"Seriously Vel, what does he have you doing?"
"Who?"
They're both in danger.

# 20:48

Cassian asks Kino if he's thought about escaping.
Tries to get information about guards and such.

"You think they're listening? You think they care enough to make an effort?"
Insists nobody's listen because they just have to spook the people into line and keep their production up.

# 22:22

Dedra briefs the ISB head.
"Axis runs a very disciplined operation, and one that's large enough to not be reliant on any one network or supplier."
They see a connection between Aldhani and Andor finally.

# 24:06

In prison, at the exchange of shifts they learn a whole 2 shifts (100 men) were killed.
One kapo tells Kino that they were making trouble.
Melshi, the downer, says "they set them all free."
Kino starts to punch him.

Cassian interrupts.
"We need to be careful. The less they think we know, the better."
Kino then tells everyone to shut the fuck up.

# 26:08

Syril gets nitpicked by his mom, realizes she's lied about searching his private things.
When he says he's been promoted (lie?).
Her attitude changes.

# 27:50

Back on shift, the prisoners hustle.

# 28:39

Vel has dinner with Mon and Perrin and their daughter Leida.
Leida says that Tay was Mon's old boyfriend, something Perrin told her.
Perrin makes a joke that at least Vel hasn't gone "political."

Mon tells Vel to "be a spoiled rich girl" to help her keep her cover.

# 30:25

Bix is a mess from her torture.

# 30:48

Syril confronts Dedra in front of the ISB building.
He's slightly obsessed with her.
He thinks he can be involved in the investigation and he wants to get Cassian arrested.

# 33:00

In the ISB building, they picked up some random rebel pilot with stolen tech.

# 33:32

Tay talks to Mon and makes it clear that she's screwed up her accounts.
He explains they need someone powerful but sketchy.
His contact wants to meet Mon.
Mon is unsure about this.

# 35:34

In the ISB, they discuss the captured pilot and how they're going to leave a trap

# 37:04

In prison, Ulaf isn't doing well.
They try to cover for him.
He needs a doctor after collapsing.

When the doc arrives, he says Ulaf has had a massive stroke.
Has to be put down.
Kino is bothered by this.

"He's lucky. He'll pass peacefully which is more than I can say for the rest of us.
  Another week like this and you'll be begging for what he's getting."

Cassian asks what happened on level 2.
Med tech doesn't answer really.
Kino presses.
Med tech says a person who got released got put on another floor so the guards fried them all to keep it quiet.
Kino finally answers how many guards are on each level.
