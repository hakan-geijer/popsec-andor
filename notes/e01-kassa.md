# 1:12

The episode opens on the urban planet Morlana One with Cassian walking along on a rainy night with his hood pulled up.
He continues through a red-light district to a lounge where he's patted down by a security guard.

# 2:35

Inside, he sizes up the bar before ordering from an bartender.
A man there with what appears to be his friend tells Cassian not to make a pass as she'd "send [him] home cryin'."

# 3:09

A hostess introduces herself to him and begins to flirt and solicit the establishment's services.
The man who'd previously talked to Cassian gets upset that the hostess spoke with Cassian first.
Cassian suggests she tend to them because they're corporate guards in a company town.

# 3:59

When asked what he's looking for, he says "a friend of mine said there was a girl from Kenari working [there]."
She inquires why he's looking for a woman from Kenari, and infers that he too is from Kenari.

# 4:39

When the hostess goes off to check, the man from earlier tries to pick a fight and Cassian engages with him and tries to stare him down.
The hostess returns saying a woman from Kenari was there but left a few months ago.
Cassian asks where she went.
The husband wants to know who he is and why he wants to know.
Cassian says the woman he's looking for is his sister.
She somewhat sympathetically says that she's disappeared (not saying whether her absence was a departure or something more nefarious) and that Cassian should leave.

# 5:21

Cassian asks for his sister's name, and the hostess incredulously replies "Nobody here gives their real name."
Cassian leaves.

# 5:56

The two guards follow him and after a ways shout out at him that he's under their jurisdiction and needs to provide ID on penalty of fine for being an undocumented visitor.
They laugh about how he's not so tough against their authority.
Cassian tries to bribe them with the credits in his coat after they draw a gun on him.

# 7:35

While they're searching Cassian, he fights back.
One guard goes down, and the original has his blaster stolen.
Cassian mocks the guard at gunpoint saying "Tell me what to do! Let's hear it boss!"
He tells the guard to pick up his friend, and the guard realizes his buddy is dead.

# 8:32

After panicking over his colleague's death the guard raises he hands and realizes that he tables have turned.
Cassian can't let him go because he knows Cassian's face, so the guard pleads for his life and tries to construct a scenario where he takes Cassian in and tells the brass it was an accident.
Cassian shoots him in the head, runs to his spaceship, and flees the planet.

# 10:08

On Ferrix, we see a rundown working class city with Cassian asleep in a different derelict ship in a junk yard.
His robot tries to wake him and calls him alternatively Cassian and Kassa.

# 11:22

We flash back to Kenari where a young Kassa lives among a low-tech tribe in a jungle.
They watch a space freighter come crashing down some distance away.

# 12:36

B2EMO, the robot belong to Cassian's adoptive mother Maarva, asks where he was, and Cassian deflects.
Cassian asks who came by, and B2EMO says Maarva and some others, and that Brasso was looking for Cassian.
Maarva told Brasso that Cassian was "out ruining [his] health and reputation with friends of low character" and that Cassian he was "sooner or later going to get [himself] himself into trouble [he] couldn't talk [his] way out of."

# 13:30

Cassian says that B2EMO needs to lie for him despite lying requires significant power.
B2EMO says it has adequate power reserves for a lie.
Cassian says that B2EMO needs to not say that it didn't see Cassian and that it doesn't know where he is.
B2EMO replies "That's two lies."

# 15:29

Cassian walks through town to catch a shift at some sort of manual labor job.
He meets Brasso who said he came by for him, and Cassian instructs him to make an elaborate lie about why he wasn't at work.
Brasso picks up the lie and continues it with more details.

# 17:22
Back on Morlana One, Syril Karn stands at rapt attention delivering a report of the two guards' deaths to his superior Chief Hyne.
When discussing one of the victims, Hyne mentions that he was surprised he wasn't killed early on account of being very unpleasant.
In the report, it's noted that the suspect, Cassian, was asking about a Kenari who may have worked there.

# 18:44

Syril continues about how much he wants to investigate, and Hyne tells him to stop.
He says the cases was "a regrettable misadventure."
He rightly identifies that the two men were killed in a fight after going to a brothel they shouldn't have been in nor should have been able to afford on their wages (implying known bribery) and drinking contraband while on the clock.
He constructs a plausible lie to cover the case so that the internal corruption and poor performance wouldn't be uncovered if the actual investigation came to light.
<!-- TODO: don't get killed in a way where no one will avenge you -->

# 19:55

Hyne describes the incident as "bad timing" because he is on his way to an Imperial regional command review.
He wants the speech and crime rates to be such that the Empire doesn't think about the Preox-Morlana Authority which benefits their superiors.
<!-- TODO: don't get killed in a way that politically hinders those with power -->

# 21:24

Back on Ferrix, Cassian goes to visit Bix Caleen at her job where she works with her partner Timm Karlo.
He's casually greeted by Timm who casually sends Cassian off to see Bix with a bit of exasperation and without saying hello.
He asks if her "special friend" can buy something off him immediately.
She says he does batches, and when Cassian insists, she realizes he's done something *bad* enough to warrant expediency.
When she asks what Cassian could have that her contact would want, he hesitates before naming some rare Imperial component that he illicitly acquired.
In their discussion, Bix accuses Cassian of holding out on her by not mentioning the part, and he retorts that she's been skimming off the top.
Bix wants her to sell the part to him, and Cassian says he isn't looking for a partner.
<!-- TODO: adversarial alliances? -->

# 23:20

Timm comes out and sees the two of them having a heated discussion and seems put off by their intensity and implicit intimacy.
Cassian notices Timm's demeanor and asks Bix if he knows anything about their black market sales.
She responds "He'd do anything for me."
Cassian isn't satisfied with this answer, and she clarifies that Timm knows nothing.

# 24:33

As Cassian leaves, Timm says "She seems upset" and "seems like that happens every time you come around."
After a short back and forth, Cassian leaves with Timm seemingly more agitated.

# 24:56

We see a young Cassian with his tribe preparing to go on a hunt to investigate the crashed freighter.

# 26:45

Syril continues to follow up on leads and notices that Cassian's ship went through a checkpoint uninspected and instructs a subordinate to follow up.
He comments on how the borders seems to be lax and that no one seems to care.

# 28:10

On Ferrix, we see that Cassian owes money to Nurchi who tries to strongarm him into repayment, but he talks his way out of it.

# 29:24

Bix hurries out of the shop to vaguely take care of errands.
Timm is unsatisfied with this answer and follows her but loses her as she duck through the alleys of Ferrix.
Bix arrives at her destination, she asks for a particular part, and two techs look at each other conspiratorially before sending her to the back to make a pirate transmission.

# 31:50

On Morlana One, the Pre-Mor security staff are slacking and pull themselves together when Syril enters the command center.
He asks for updates and knows the Cassian is on Ferrix.
When suggesting that they put out a broadcast for information about an unknown Kenari, an employee points out that while it's technically under their jurisdiction, "they have their own way of doing things."
When telling to put out a flood of bulletins and a desk to monitor it, the staff react without enthusiasm.
<!-- TODO: de facto vs. de jure control -->
<!-- TODO: unmotivated enemy -->

# 33:10

Cassian is repairing the ship he used to fly to Morlana One and is swapping out the transponder.
He's caught by a guard who was turning a blind eye to let Cassian steal the ship for a night.
The guard has run out of patience with Cassian and told him to not come back.
<!-- TODO: making enemies makes it hard to go to ground. don't use up all your favors -->

# 34:47

The tribe leaves for the freighter, and Cassian and his sister share a look good bye expecting to see each other again.
<!-- TODO: always say good bye -->

<!-- TODO: Syril's blatant and open disobedience could have gotten him caught before he finished the investigation "cat's away, mice play" isn't actually accurate -->
