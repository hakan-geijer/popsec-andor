\section*{E02: That Would Be Me}

\subsection*{Finding the Leak}

Cassian arrives at Maarva's hovel where she confronts him about the Pre-Mor security bulletin looking for a Kenari in relation to the deaths of two guards.
Their conversation is so important, it's worth repeating in full.

\begin{xltabular}{\textwidth}{l X}
    \textsc{Maarva}  & Who else knows? \\
    \textsc{Cassian} & About what? \\
    \textsc{Maarva}  & That you were born on Kenari. \\
    \textsc{Cassian} & You don't want to hear what happened? \\
    \textsc{Maarva}  & Mhmm, we'll get to that.
                       But who knows?
                       Who have we told? \\
    \textsc{Cassian} & I don't know, I--- \\
    \textsc{Maarva}  & We have always said Fest.
                       Every doc I've ever submitted has always said you were born on Fest.
                       Have you ever said anything other than Fest? \\
    \textsc{Cassian} & Officially, no.
                       I don't think so.
                       But people, yes. \\
    \textsc{Maarva}  & Who? \\
    \textsc{Cassian} & Uh\ldots{} And so did you. \\
    \textsc{Maarva}  & Well, how many? \\
    \textsc{Cassian} & I don't know.
                       It's not something I've been keeping track of! \\
    \textsc{Maarva}  & Well, everyone I've told is dead. \\
    \textsc{Cassian} & That's ridiculous. Jezzi. Sammo. Hmm? \\
    \textsc{Maarva}  & That's family. \\
    \textsc{Cassian} & If we're making a list, we're making a list. \\
    \textsc{Maarva}  & Well, it's all your women that I'm worried about. \\
    \textsc{Cassian} & Oh, come on. Please.\\
    \textsc{Maarva}  & Femmi, Karla, Sondreen. \\
    \textsc{Cassian} & Stop. Stop! \\
    \textsc{Maarva}  & There are some names I don't even know! \\
    \textsc{B2EMO}   & B-B-Bix. \\
    \textsc{Cassian} & Bix has nothing to do with this. \\
    \textsc{Maarva}  & But then who told these Pre-Mor bastards about Kenari? \\
    \textsc{Cassian} & \textit{(pause)} That would be me. \\
                     & \textit{(Cassian argues with B2EMO)} \\
    \textsc{Maarva}  & Cassian, what have you done? \\
    \textsc{Cassian} & I messed up. \\
\end{xltabular}

Their conversation tersely goes though many of the phases of people's reactions to finding out there's been an information leak.
Cassian starts by trying to make excuses as if his mother's anger is about why he's being pursued, not that he wasn't careful.
Often when we make OpSec mistakes, our instinct is to justify them based of intents or results and not acknowledge that they happened and try to resolve the issue or perform a root cause analysis.

Maarva is right to quickly jump to figuring out who might have leaked the information.
It would help them figure out who can still be trusted and what avenues exist for keeping Cassian safe.
Cassian says he hasn't been keeping track of who he's told about his true identity, and that is precisely one of the problems with having multiple alias and identities.
We generally see ourselves as one complete person, and it can be hard to remember with whom we've shared what version of ourselves.
Online this can be easier because of the simplistic and stunted interactions we have with others, but offline this can be far harder.

His mother's criticism that he probably shared his identity with too many lovers is extremely valid.
We often mistake passion and intimacy for trust, and even aside from our activist lives, most of us have probably been burned by an ex who knew too much about us and who either maliciously or innocently didn't hold their tongue.
However, she also brushes off her indiscretions as wise and prudent while Cassian's are rash an foolish.
He in turn is right to actually make the full list of who knows his true identity because the leak coming from within their circle of trust is plausible.

Over the course of the conversation, he realizes that having said he was looking for his Kenari sister while on Morlana One was the source of the leak.
Admitting you've made a mistake and putting your ego aside is critical for OpSec.
We all fuck up and have weaknesses, and being able to identify and work around them is the only way forward.
If we lie to ourselves and others, we will take ill-informed actions.

\subsection*{Covering Your Tracks}

At the start of the episode, we see Bix at work reading a terminal.
It shows her the bulletin from Pre-Mor asking for information about Cassian.
Her coworker and lover Timm comes in, and she hurriedly closes it before declining his invitation for dinner and zipping off to warn Cassian.
Timm turns the terminal back on and sees what she was looking at.
This piques his interest again, and he secretly follows her as she leaves.

Her decision to turn the terminal off when Timm walks in is based on her knowledge that Timm is suspicious of Cassian and their relationship, but in her hurry she doesn't completely wipe what she was doing.
This small detail was enough to cause the info about the hunt for Cassian to spread a little further in particular to someone who's already suspicious.

Closing apps on our phones, using private browsing, and cleaning up printed materials when we leave a space are all trivial tasks, but they stop snippets of info from leaking
When we go to show a friend a funny picture, an important conversation might pop up when we unlock our phone.
When we spontaneously bring someone back to our flat, sensitive materials might be scattered on our desk, and people tend to be curious.
All this information might get out anyway, but it's better to slow the spread.

\subsection*{Patriarchal Jealousy}

In the first episode after Cassian argues with Bix about selling directly to her fence, Timm confronts him as he leaves and gets protective of Bix, and we later see him following her around town.
It's implied by what B2EMO says when Cassian is arguing with Maarva about his lovers being a source of the leak that Cassian had an affair of some sort with Bix prior to her starting one with Timm, and this is confirmed in E07.
Timm exhibits classic signs of patriarchal protectiveness over ``his'' woman.
Bix was right to say ``he'd do anything for me'' when Cassian asks if he knows about their crimes.
She meant it in the sense that he would protect her and that which is important to her from external threats, but the truth is that Timm would do anything \textit{to have her for himself}.
When he follows her to a cantina after seeing the Pre-Mor bulletin, he sees Bix having a seemingly intimate conversation with Cassian.
He wants Cassian out of the picture, so he snitches to Pre-Mor causing them to send a snatch squad.

As the saying goes, misogynists make great informants.\footnotemark[6]
Timm's passion and dedication aren't to the ideals that Bix and Cassian share, about the joys of ripping off the Empire to make a few credits to survive.
He simply wants to have Bix as his own.
Bix and Cassian are criminals, not revolutionaries at this time.
In fact, we know their criminal relationship is slightly adversarial where they hold out on each other for personal gain.
This is a case where if they were more altruistic and had developed a coherent politic of mutual aid or liberation, Timm's possessiveness would have been revealed for what it is.
Their selfishness made it impossible to see his and what a threat that was.

\footnotetext[6]{See the zine \textit{Why Misogynists Make Great Informants: How Gender Violence on the Left Enables State Violence in Radical Movements}.}

It is not enough to do the right things.
We have to also do them for the right reasons, not just from an ethical perspective, but because the analytic tools that come from well-developed liberatory theories allow us to spot false allies and snitches easier.

\subsection*{Others' Secrets}

As we see later in E03, Timm knows Cassian is from Kenari because Bix mentioned it to him.
Cassian was right to trust Bix to not use that knowledge against him, but every person we tell our secrets to is another person that might let it slip.
It's easier for us to keep track of what knowledge about our own lives is secret, but it's much harder to do so with information about the lives of others.
Keeping our whole lives private from others can be unsustainable, and a way to help minimize information spreading like this is to be extremely explicit about what information of ours is secret and under what conditions it can be shared.
These conversations might feel awkward or too formal, but they protect us from accidents.
