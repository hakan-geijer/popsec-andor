\section*{E08: Narkina 5}

\subsection*{Finding Cracks}

After being sentenced, Cassian is shipped off to a labor camp on Narkina 5.
The facility is drilled into the ocean with artificial whirlpools around it to suck in anyone who tries to swim away.
Prisoners are not allowed to have shoes, and the electrified floors serve as the primary means of control.
The facility is austere, and control is absolute, but it is not brutal because the prisoners are needed for labor.

As he's being escorted from the arrival platform to his section of the factory, he notices the guards are lackadaisical about their duties and are going through protocol by rote.
One guard criticizes another for arriving without his partner then begins the intake procedure without the required second armed guard.
When the second guard does arrive, he mentions that they're shorthanded elsewhere.
Cassian knows about the apathy and disarray of the facility.

Repression may be brutal, and it may seem total.
It aims to demoralize us and make use believe escape, change, and a better world aren't possible.
But there are always cracks in the behemoth.
The belief that one's control is absolute breeds complacency.
Bureaucracy can create loopholes.
The panopticon requires maintenance, and it will break down.
Maybe there isn't a way to revolt today, but if we stay observant, we can find the patterns, the cracks, the small ways that repression can be overthrown.
Be observant.

\subsection*{New Circles}

After induction and his first shift, the prisoners go back to their cell block.
Jemboc, one the prisoners, shows Cassian how to work the utilities in his cell while others start to listen in impatiently.
Jemboc asks what he did to get sent to prison, and Cassian says ``Nothing.''
Another prisoner says ``Ask him already,'' and Jemboc asks him if he knows about the Public Order Resentencing Directive that caused their sentences to double the month before.
He's also asked if he knows about the attacks against the Empires that led to this change.
He says he doesn't know anything, and the other prisons voice their upset that some rebel cowboys attacking the Empire has caused them to have to pay the price.

Compared to his cavalier attitude about who might know what about him when we first encounter him back on Morlana One, Cassian has learned to keep his mouth shut, and rightfully so.
Before being imprisoned, when his mother speaks of how thrilled she is about the attack, he doesn't mention his involvement.

Knowledge is power.
What someone knows about us can often be used against us.
Moreover, giving up information should be done only when we know \textit{why} (and nearly fully so) someone wants to have that knowledge.
Often it's just curiosity or small chat, but sometimes if you give someone a bit of information they'll tug on it and your cover story will unravel or you'll find yourself backed into a corner.

\subsection*{The Dangers of Sympathy}

On Ferrix, Cassian's mother is ill and being attended to by the family friends Brasso and Bix.
Outside, they converse about Cassian being missing, and afterward Bix goes to Salman Paak's shop to use his secret radio to call Luthen in hopes of reaching Cassian.
Salman tells her ``Not sure that's a good idea.''
When she says in a strained tone that it's urgent, he acquiesces.
Ferrix is a small working-class community, and we can assume that Salman knows about Maarva's health.
He's had the radio powered off for security reasons, but his sympathy gets the best of him.
Bix makes the call, and the next morning finds that Salman had been snatched by the Imperials.

One of our strength as anarchists---our sympathy for others, our altruism---can get the best of us.
The thing that is obviously kinder, that should be done if there were no security implications, can sometimes by something that harms us.
Most of us are lucky enough to have never found ourselves in a position where we had to deny someone kindness on the grounds of security, but as the world grows more tumultuous, we may find ourselves having to be harsh in the name of safety.

Letting someone use your phone to make a call to alert their loved ones they're safe knowing that your line may be tapped.
Letting someone borrow a car to drive to see a dying friend knowing your plates will be seen at that address.
Letting someone publish a heartfelt text in your anthology knowing it risks you being charged with inciting insurrection.

I hope we never find ourselves having to make these tough calls, but one day we may have to, and we have to be both prepared to make such decision and to accept them when someone else does.
This is not saying we have to harden ourselves against our own desires or our desires to help others.
I'm just saying that our heart can lead us to places our brain knows are dangerous, and sometimes we have to turn away from those choices.

\subsection*{When It's Too Personal}

On Coruscant, Luthen walks into the shop's back room where Kleya listens in on Bix's call without responding.

\begin{xltabular}{\textwidth}{l X}
    \textsc{Kleya}  & It's the shop owner on Ferrix.
                      She's trying to find Cassian Andor.
                      His mother is ill.
                      We're not answering. We can't. \\
    \textsc{Luthen} & She could point us in the right direction. \\
    \textsc{Kleya}  & She's asking us. \\
    \textsc{Luthen} & She knows more than we do. So much more. She might have a lead. \\
    \textsc{Kleya}  & More likely it's the ISB working her radio. \\
    \textsc{Luthen} & You're guessing! \\
    \textsc{Kleya}  & And you're slipping!
                      We're shutting down Ferrix. The code, the frequency, all of it.
                      I'm thinking clearly, and you're not.
                      Tell me to shut it down. \\
    \textsc{Luthen} & It's Andor. Knowing he's out there, knowing me, not knowing what he knows.
                      I took him on the \textit{Fondor}.
                      Was I insane? \\
    \textsc{Kleya}  & You were desperate for Aldhani to work, and it did.
                      And we'll find him, just not like this. \\
    \textsc{Luthen} & Vel was out hunting. She and Cinta. Are they in Ferrix yet? \\
    \textsc{Kleya}  & I'll have a listen. We're being extremely careful with it. \\
    \textsc{Luthen} & The woman's name is Bix Caleen.
                      Vel could have a look if it's safe.
                      The know what they're doing.
                      I'm not slipping, Kleya. I've just been hiding for too long. \\
    \textsc{Kleya}  & It's all different now. We're going loud. Vulnerability is inevitable. \\
    \textsc{Luthen} & I'm not slipping. \\
    \textsc{Kleya}  & I know. I just need you to wake up. There's a lot to do. \\
    \textsc{Luthen} & Shut it down. \\
\end{xltabular}

\begin{figure}[htb]
    \centering
    \includegraphics[width=\textwidth,keepaspectratio]{./img/luthen-kleya}
\end{figure}

This exchange captures well how we can get too wrapped up or emotionally invested in our political projects to the point where we can't see straight.
Luthen knows that Vel is hunting Cassian to tie up the loose end that connects him back to the robbery.
He is so laser focused on killing Cassian to prevent his own name from leaking that he misses the risk of using the radio.
Cassian isn't actively malicious as Luthen knows because Cassian didn't run off with the money when he had the chance, but Luthen rushes anyway.
He feels personally endangered by Cassian knowing his name more so than the acute threat from the ISB if he used the radio.

We often let our fears get the best of us or let strong emotions push us.
We're only human after all, but these hotheaded moments can have devastating consequences not just for us but for our comrades too.
Often all we need to do is slow down, take a breath, and attempt to accurately asses our risks and what strategies we have available to counter them.
Showing up at an ex-comrade's flat because they threatened to doxx you might be what your heart cries out for, but that might put you and your whole crew at greater risk.
Making a snap decision to strike back at some group that wronged you might satisfy your craving for righteous revenge, but without recon, you might immediately get caught.
But maybe it's something simpler like using your sock account to ask a question to an abuser, a question that is a little too particular and gives your identity away.
These aren't statements against doing any of those things.
It's just that our actions don't exist in a vacuum, and like Luthen we need comrades who can help keep us in check from violating our crew's established security guidelines.

\subsection*{Counter-Intel}

Luthen flies to Segra Milo to meet with Saw Gerrera, the leader of an extremist group in the milieu of rebel factions.
The first part of their conversation is Luthen insinuating (with praise) that Saw was the one who attacked Aldhani as Saw tries to draw it out of Luthen that he might have been behind the attack.
When the conversation moves on, Saw seems to believe that Luthen wasn't involved.
Nominally, the are more on the same ``side'' as each other than not, but if Saw doesn't need to know, then it should remain a secret.

Keeping quiet about all of one's activities is one strategy for preventing others from what's going on.
This works for some.
Luthen as a coordinator naturally has to talk and schmooze with many people, something total secrecy might inhibit, so he takes the approach of jovially sewing misinformation not just about his activities but what information he has.

Within our radical milieus, there are many ideologies and even within a single ideology many factions.
Together we call this ``the left,'' and just like the Rebel Alliance, it is coalition more than a formal alliance.
Factions may turn on each other, and our comrades may betray us.
The further one is from anarchism both in terms of ideology or community, the more likely they are to eventually turn on us to gain advantage.
Leaving a chaotic trail of misinfo about one's personal details or deeds can prevent many of these repercussions while still allowing one to operate effectively.

\subsection*{Uneasy Alliances}

The second half of Luthen's conversation with Saw is him trying to convince Saw to lend air support to another faction so that they can make a combined hit on an Imperial power station.
In rage, Saw tells off Luthen for calling his adherence to his own ideology as ``petty differences'' and says that he's not risking his people for someone else.
Saw has a reputation for being an extremist, and Luthen knows that his operation is well-funded and successful.
Saw, however, is right to refuse the tactical alliance.

Saw is an anarchist,\footnotemark[9] and like us, his vision of the future is not some vague ``anti-Imperialism.''
Anarchists are neither the shock troops nor the canon fodder for some larger struggle.
We have our own program and beliefs, and where these line up we will take advantage, but often explicit alliances are dangerous.
Not even just historically but also currently anarchists have been betrayed by the other ``left'' groups they work with, and when we do work together we are expected to bow to authority because we are the minority of cranky others.
To prevent ourselves from being sold out or used up by allies who want to (falsely) get a quick win with established power, we need to play our game close to the chest.

\footnotetext[9]{Yes, literally. They say it in the show.}
