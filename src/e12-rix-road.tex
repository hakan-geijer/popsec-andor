\section*{E12: Rix Road}

\subsection*{False Evidence}

Outside a soirée, Mon waits for Perrin inside the airspeeder.
When he gets in, they begin to tiff, and Mon asks their driver to mute the intercom for some privacy.
She begins accusing Perrin of gambling again, and illegally at that.
He denies it, and she starts to ask how he even got the money.
Naturally, their driver listens in.
Mon knows he'll do this because she knows, or at least very strongly suspects, that he's an ISB agent.
In a later scene, this is confirmed when he reports that Perrin gambles and may be misusing funds to do so.

My speculation is that Mon is feeling the heat so bad that she's desperate to throw them off her trail by sending them chasing a false lead so that there's a plausible reason for the money to be missing.\footnotemark[11]
Her motivation for doing so might be to ensure only she goes down if they do start investigating and that the rebellion is safe.
Doing this is risky because the ISB wasn't really on to her yet, only suspicious, and now they're actively moving against her armed with something they didn't have before.

\footnotetext[11]{
It's unclear whether she actually believes he's gambling or not, and that's somewhat irrelevant.
He has a history of doing so which may or may not be known to the ISB, but any search would likely turn up corroborating evidence.
}

Often we think that we can outsmart repression apparatuses by being tricky.
When we're successful, it's usually because we've managed to provide so little evidence that there was no way to prosecute much less convict us.
It's far more rare that an active counter-intelligence plan works.
This is largely because we don't have insight into their workings.
We can't tap their calls, and we don't have inside agents.
All our assumptions are by looking at patterns of prosecutions (what evidence, what laws, what tactics) or other patterns of repression.

In Mon's case, she doesn't actually know anything.
She doesn't know where they're on to her or what evidence they have or don't.
Vel, who was unmasked while assaulting and robbing an Imperial garrison has been in her house multiple times since the attack.
Anything that draws attention carries more risk than potential benefits.

Whether this pans out for Mon in the second season remains to be seen, but our lives aren't flashy cinematic productions.
Plot armor won't protect us.
Leaving chaos behind us to obfuscate our actions is a reasonable tactic as is flying as stealth as possible.
Drawing attention to ourselves as part of a counter-intel scheme is generally inadvisable.

\subsection*{Security Culture and Spontaneous Rebellion}

On Ferrix, the various factions prepare for the funeral.
The Imperials think it will be a small affair in the early afternoon as does Luthen, Vel, and Cinta.
Cassian works with some compatriots to move toward the hotel to rescue Bix, and the local thug Nurchi schemes to sell him out.
Syril and Linus arrive to do their own op against Cassian, and Dedra arrives separately to oversee the operation.
However, hours before things are supposed to go down, we see what appears to be thousands of Ferrixians converging from multiple separate fingers to form a larger protest.
The Imperials and Rebels are caught off guard.

\begin{figure}[htb]
    \centering
    \includegraphics[width=\textwidth,keepaspectratio]{./img/maarva}
\end{figure}

OpSec is both the personal actions one takes daily to protect their identity and the measures a small crew takes on a per action basis to secure their activities.
Security culture is the broad set of norms within a milieu or extended network that foster wide-spread OpSec and solidarity.
It can mean never snitching or talking to cops.
It can mean normalizing phoneless organizing meetings.
Maybe it's just whistling when patrols come through the area to alert everyone doing shady thing to cut it out for a few minutes.

We know that the ISB has agents in Ferrix observing, and the Rebels have been there for weeks watching for Cassian, but they don't know about the Ferrixians' plans.
Snitches haven't ratted out the plans yet because \textit{they simply do not know}.
What sort of community---in the sense of interlaced social networks---have the working class Ferrixians built that would even allow this to happen?
I can't give a solid answer to this either, but there are some things that work at smaller scales.

When one genuinely cares about the well-being of another, an injury to them is akin to an injury to oneself.
Ratting someone out leaves no gain because it is also a form of self-harm.
When we have tight connections to those around us---or rather when we have a strong social network---we know who to trust and who not to.

We often talk about ``community self-defense'' but what is often lost is the idea that community---again, in the sense of networks---\textit{is} self-defense.
A small militia won't protect a neighborhood that is simply people living in proximity to each other.
But a neighborhood that cares for one another has less need for a specialized militia when everyone is involved in its defense.
On a smaller scale, you can't keep your apartment block from being sold off on your own even if you and your flatmates are the most hardcore insurrectos this side of the Atlantic.
Four people can't defend 400 who aren't invested in their own liberation.
But those four could be the start of a radical network that learns to defend itself.

When we think of the seemingly spontaneous insurrections that occur around the world in response to oppression or some injustice, they don't happen because of some anarcho-vanguard throwing the first stone or everyone waking up one day and deciding to rally behind a cause they previously didn't care about.
They come from shared ideals, mutual aid, solidarity, and strong bonds between individuals.
If you're looking to recreate the Rix Road riot and tip the balance of power in your city, one of the best armaments you can have is a robust social network.
These networks give you cover to organize and protect you when repression comes.
