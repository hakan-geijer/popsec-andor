\section*{E05: The Axe Forgets}

\subsection*{Trust, but Verify}

After Cassian's first night at the camp, he awakes to his possessions missing.
Across camp, his gear is spread out on a workbench as Arvel washes himself beside it.
Cassian is upset that his new comrades would rifle through his things, but before he can speak Arvel tells him that Vel had asked him to take a look and that the stakes are high.
In particular, Arvel is suspicious of the Pre-Mor service revolver that Cassian had nicked from the guards he killed in the first episode.
They also have a back and forth when Arvel sees that Cassian has noticed his prison tattoos.

The members of the cell are right to be suspicious of someone new who will eventually hold all their lives in his hand.
Vel might trust Luthen, and the others might trust Vel, but when the consequences of a security fuckup are death or life in prison, they need the peace of mind of having looked themselves.
It might not prove conclusively\footnotemark[7] one way or another that Cassian is or isn't an Imperial agent, but it can be evidence in support of their doubts or evidence of Cassian's authenticity.

\footnotetext[7]{
Evidence like this can be fabricated.
An interesting and well-known case during WWII is the OSS's Operation Mincemeat where they put convincing personal possessions on a corpse with fake intel and let it get picked up.
It fooled the Germans into leaving Sicily poorly defended when the Allies invaded from North Africa.
Or perhaps more close to home, an undercover cop is able to copy local antifascist fashion (to a reasonable degree).
}

This tactic may seem invasive or even extreme to novice activists, but this pattern is not unfamiliar in radical communities.
Presence at community events or simply ``knowing a guy'' isn't sufficient to establish trust with clandestine groups.
Often at a minimum, such groups will use OSINT methods to research new people and might even conduct make the person submit to a background check they run themselves.
As Arvel noted, one can't be too cautious when the stakes are so high.

\subsection*{Reliance}

After his encounter with Arvel, Cassian sits down to breakfast with Nemik.
Nemik pulls out some complicated navigational contraption.

\begin{xltabular}{\textwidth}{l X}
    \textsc{Cassian} & That's an old one. \\
    \textsc{Nemik}   & Old, and true. And sturdy.
                       One of the best navigational tools ever built.
                       Can't be jammed or intercepted.
                       Something breaks, you can fix it yourself. \\
    \textsc{Cassian} & Hard to learn. \\
    \textsc{Nemik}   & Yes, but once you've mastered it, you're free.
                       We've grown reliant on Imperial tech, and we've made ourselves vulnerable.
                       There's a growing list of things we've known and forgotten, things they've pushed us to forget. \\
\end{xltabular}

\begin{figure}[htb]
    \centering
    \includegraphics[width=\textwidth,keepaspectratio]{./img/nemik}
\end{figure}

Nemik is a starry-eyed idealist as those who write manifestos are wont to be, and we're supposed to take this as lofty and impractical, but he is spot on with his analysis.

As I'm writing this, on the 18\textsuperscript{th} of November in the year 2022, Twitter is in its possible death throes after mass layoffs, and people are pointing to its loss as a crushing blow to activism.
The Arab Spring, Occupy, and less striking things like the ability to more easily crowdsource funds are all held up as things that were only possible through Twitter.
However, Twitter was and is controlled by entities that are actively hostile to us, and our inability to think of alternatives harms us.

Twitter is easy to use.
It's there, it works, and changing away from it is hard.
But looking back at instances where it was used to positive ends neither says that those ends couldn't have otherwise been met with other tools or that future events would require the same tools.

This isn't just about Twitter.
Mobile phones and the internet itself might be powerful and easy to use compared to their analog counterparts, but our reliance on them without having alternatives is a risk we need to contend with.
How will we communicate? Spread ideas? Create propaganda? Navigate? Do research and gather intelligence?
We should practice using alternatives where possible to avoid current repression and create possibilities in the future.

\subsection*{What Isn't There}

Back on Coruscant, ISB\footnotemark[8] agent Dedra Meero continues to track missing supplies with her subordinate Heert.
First tipped off about this by the starpath unit Cassian tried to sell to Luthen, she's come to realize that there is some sort of pattern.

\footnotetext[8]{Imperial Security Bureau. ``The Feds.''}

\begin{xltabular}{\textwidth}{l X}
    \textsc{Dedra} & He's right though.
                     It's too spread out to be organized. \\
    \textsc{Heert} & But you don't believe that. \\
    \textsc{Dedra} & I know this.
                     If i was them, this is how I'd do it.
                     I'd spread it out.
                     Never climb the same fence twice. \\
    \textsc{Heert} & It's too random to be random. \\
\end{xltabular}

From Dedra's end, all she can see is missing supplies from manifests.
She even has the eye to guess which reports are falsified in the bureaucratic hell of an interplanetary empire.
While there is no pattern of some cluster of targets that are repeatedly hit for silent thefts, there is a pattern of perfect spread.
True randomness would have some duplication, and Dedra is able to read this as intentional non-randomness.

The evidence we leave behind aren't just our footprints but where there are no footprints at all.
For example, in today's society it's more suspicious to have no presence on the internet than an extremely dull presence.
Everyone in a milieu turning their phones off right as they reach a certain part of the woods is itself a signal.
When we try to leave no trace, it can be better to leave a false trail than no discernible trail at all.

\subsection*{Unadmitted Fear}

While the rebels are taking a break on the way to the Imperial garrison, Arvel holds a knife to Cassian's neck and takes the gem Luthen had given him as a down payment for his services as a mercenary.
He holds it up as vindication of his suspicions that Cassian isn't who he says he is.
After listing off all of ``Clem's'' incongruities, Arvel says ``I need to know who I'm riding with.''
Tensions rise, and Vel diffuses the situation saying they'll talk about it at camp.

Cassian admits he's being paid, and says he doesn't want to walk in looking over his shoulder.
He then calls out Arvel for losing his nerve about the mission and latching on to something to deflect his fear.
If he can't admit he's too afraid to do the mission, he can find something that's wrong enough to make aborting the only logical choice.

We too fall into this trap.
It's easier to turn our sights inward where the only consequences are words than to carry out actions that could potentially derail our lives.
Sometimes we do this under the alleged auspices of security culture, but this misuse of the processes of OpSec to justify our fears harms not just a current action but security culture at large.
Nitpicking about perfect adherence to some tenets of social justice or calling everyone you haven't known for a decade a possible cop is almost always easier than doing the work, especially the scary work.
This isn't to say we should do as Arvel does and work with mercenaries that just got dropped on us, but we should ask ourselves if we're attacking and over-scrutinizing fellow travelers out of genuine concern or if it's away of avoiding what needs to be done without ever having to admit our own fears.
