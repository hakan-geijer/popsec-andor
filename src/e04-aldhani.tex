\section*{E04: Aldhani}

\subsection*{Aliases}

Luthen and Cassian arrive on Aldhani where Luthen explains to him some of the details of the heist against the Imperial payroll held at the local garrison.
Luthen tells him to pick a name to use with the people he's about to meet.
Cassian picks Clem, his adoptive father's name.
After getting pinched by the cops because of his past connection to Kenari, Cassian should be more wary about choosing a name that can be linked to him.

A common error when using pseudonyms or constructing aliases is that we want them to still have a bit of ourselves in them.
We might use something like an old nickname from our childhood or even our pet's name.
Any misdirection is helpful, but making the leap to something completely random and nondescript makes it harder to link your identities.

\subsection*{Personally Identifying Information}

After arguing with Luthen about the merits of taking on a new member, Vel accepts Cassian and they begin walking to the rebel camp.
Cassian is curious about Luthen.
Vel says over course of the conversation ``You should have asked him that when you had the chance,'' ``He is something we will never discuss,'' and ``We never mention him {[to the others]}.''

It is not her decision to reveal information about Luthen.
Only he can do that, and without some long-standing rule about what can or cannot be said or some explicit agreement in this case, Vel takes a sensible default of silence.
Luthen may not fully trust Cassian, and Vel may not either.
There's some risk of Cassian being captured, and the less he knows the better.
Likewise, as some sort of wealthy benefactor, the grunts who risk their lives in the mud for the rebellion might resent his existence if they knew much about him, so it's better that he effectively doesn't exist.

This sort of silence is common practice among anarchists.
Not even just when someone new is around, but also among trusted friends, we don't spill or comrades' secrets just because someone asks.
Sharing information about someone's identity, origin, or activities can be dangerous, and more importantly it is a matter of consent.
When personal information is shared, it is done so with only those it was directly shared with and without any implied consent for it to be spread further.

\subsection*{Vouching}

A similar scenario as just discussed replays after Vel and Cassian arrive at the camp.
The other rebels are generally not keen on someone being added so late, and when questioned about it Vel hides her relation to Cassian and only says that he comes highly recommended and that she trusts him.
She trusts Luthen, and since she can safely assume that Luthen has done his research, by proxy she should also trust Cassian.
In this case Vel brokers trust between Luthen (who is unknown to the rebels) and the rebels themselves so as to hide Luthen's connection and also Cassian's past.

When operating in small clandestine affinity groups, we often have connections to others.
We need to somehow establish trust with these connections or when individuals join us.
However, if everyone knows everything about everything about everyone, the cell model of security breaks down.
Vouching is the process by which we can establish trust without needing all of one's information.
Typically this process is that one person serves as a trust broker and either does the research themselves or uses another trusted party who knows the individual in question quite well to establish the appropriate level of trust that should be extended to said individual.
When used well, vouching can allow trust to be established while still hiding connections from complete exposure.

\subsection*{Repression}

On Coruscant, the capital of the Galactic Empire, senator Mon Mothma arrives via airspeeder to Luthen's antiquities shop where he plays the role of a respectable and dignified curator.
Luthen's assistant Kleya points out that Mon's driver is someone new.
Mon steps in, and she and Luthen put on a ceremonious greeting for the driver as if he were a spy before they retreat to the back room under the cover of looking at newly arrived items.

Once they have privacy, Luthen asks if Mon can get him the money she's agreed to, and she replies that there's difficulty because of Empires actions.
Luthen dismisses her worries saying "Oh, they're watching everyone."
She responds that she's heavily surveilled including new staff at her bank and her driver.
She also says that she wants to bring someone new onboard.
Luthen replies that they're too vulnerable and they need money, not more people.

Both have points.
Insurrection cannot be sustained on hope alone; it needs bread too.
However, someone who is under heavy surveillance bringing a new person around carries its own risks.
It's not just whether they're an informant but if they are careful enough with their words and actions or if they could be leveraged to turn if captured.

Without the time or funds, the insurrection could fail.
Just as easily, without outside help, it could fail too.
OpSec is about risk management, not elimination.
Bringing someone on board to help might be risky, but without their help you might be dead in the water.
A balance is often struck by taking active efforts to obfuscate true plans or motivations so that those who help us can't piece together what we're doing but we still benefit from their assistance.
